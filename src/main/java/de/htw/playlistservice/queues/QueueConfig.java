package de.htw.playlistservice.queues;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfig {

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("confirmation.songs");
    }

    @Bean DirectExchange playlistsDirectExchange() {
        return new DirectExchange("exchange.playlist");
    }

    @Bean DirectExchange songsDirectExchange() {
        return new DirectExchange("exchange.song");
    }

    @Bean
    public Queue response() {
        return new Queue("response");
    }

    @Bean
    public Queue deleteUser() {
        return new Queue("deleteUserPlaylist");
    }
    @Bean
    public Queue deleteSong() {
        return new Queue("deleteSongPlaylist");
    }
    @Bean
    public Queue getUserPlaylistResponse() {
        return new Queue("getUserPlaylistsResponse");
    }

    @Bean
    public Binding playlistConfirmationBinding(@Qualifier("directExchange") DirectExchange directExchange,
                                               @Qualifier("response") Queue queue) {
        return BindingBuilder.bind(queue)
                .to(directExchange)
                .with("old.playlists");
    }

    @Bean
    public Binding deleteUserBinding(DirectExchange directExchange, @Qualifier("deleteUser") Queue deleteUserQueue)  {
        return BindingBuilder.bind(deleteUserQueue)
                .to(directExchange)
                .with("delete.user");
    }

    @Bean
    public Binding deleteSongBinding(@Qualifier("songsDirectExchange") DirectExchange directExchange, @Qualifier("deleteSong") Queue deleteSongQueue) {
        return BindingBuilder.bind(deleteSongQueue)
                .to(directExchange)
                .with("delete.song");
    }


    @Bean
    public Binding getUserPlaylistBinding(DirectExchange directExchange, @Qualifier("getUserPlaylistResponse") Queue getUserPlaylistResponse)  {
        return BindingBuilder.bind(getUserPlaylistResponse)
                .to(directExchange)
                .with("user.playlists");
    }

    @Bean
    public MessageConverter jackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
