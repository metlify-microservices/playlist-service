package de.htw.playlistservice.queues;



import de.htw.playlistservice.models.Playlist;
import de.htw.playlistservice.models.events.*;
import de.htw.playlistservice.services.PlaylistService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class QueueConsumer {

    @Autowired
    private PlaylistService playlistService;

    @RabbitListener(queues = "response", concurrency = "3")
    public PlaylistsInDatabaseConfirmationEvent receive(ArePlaylistsInDatabaseEvent event) {
        Set<Integer> songIds = event.getPlaylistIds();
        System.out.println(songIds.iterator().next());
        boolean result = playlistService.arePlaylistsInDatabase(songIds);
        System.out.println(result);
        PlaylistsInDatabaseConfirmationEvent answer = new PlaylistsInDatabaseConfirmationEvent(result);

        return answer;
    }


    @RabbitListener(queues = "deleteUserPlaylist", concurrency = "3")
    public void receiveDeleteUser(DeleteUsersEvent event) {
        System.out.println("DeleteUser");
        int userId = event.getUserId();
        System.out.println(userId);
        playlistService.deletePlaylistsByUser(userId);
    }

    @RabbitListener(queues = "getUserPlaylistsResponse", concurrency = "3")
    public UserPlaylistsEvent receiveUserSongsResponse(GetUserPlaylistsEvent event) {
        int userId = event.getUserId();
        List<Playlist> playlists = playlistService.findAllPlaylistsByOwner(userId);

        Set<Integer> playlistIds = new HashSet<>();
        for (Playlist playlist : playlists) {
            playlistIds.add(playlist.getId());
        }
        UserPlaylistsEvent answer = new UserPlaylistsEvent(playlistIds);

        return answer;
    }

    @RabbitListener(queues = "deleteSongPlaylist", concurrency = "3")
    public void receiveDeleteSong(DeleteSongEvent event) {
        int songId = event.getSongId();
        System.out.println("deleting song with song id " + songId);
        playlistService.deletePlaylistSong(songId);
    }
}
