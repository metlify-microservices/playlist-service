package de.htw.playlistservice.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @NotEmpty
    @NotBlank
    private String title;
    private int userId;
//    @ManyToMany
//    @JoinTable(
//            name = "playlist_song",
//            joinColumns = {@JoinColumn(name = "playlist_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "song_id", referencedColumnName = "id")}
//    )
//    private Set<Song> songs;
    private String genre;
    private boolean isPrivate;
    private String lastUpdated;

    public Playlist(int id, String title, int owner,
//                    Set<Song> songs,
                    boolean isPrivate, String genre, String lastUpdated) {
        this.id = id;
        this.title = title;
        this.userId = owner;
//        this.songs = songs;
        this.isPrivate = isPrivate;
        this.genre = genre;
        this.lastUpdated = lastUpdated;
    }
    public Playlist(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int owner) {
        this.userId = owner;
    }

//    public Set<Song> getSongs() {
//        return songs;
//    }
//
//    public void setSongs(Set<Song> songs) {
//        this.songs = songs;
//    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}