package de.htw.playlistservice.models.events;


public class SongsInDatabaseConfirmationEvent {
    private boolean areSongsInDatabase;

    public SongsInDatabaseConfirmationEvent(boolean areSongsInDatabase) {
        this.areSongsInDatabase = areSongsInDatabase;
    }

    public SongsInDatabaseConfirmationEvent() {
    }

    public boolean isAreSongsInDatabase() {
        return areSongsInDatabase;
    }

    public void setAreSongsInDatabase(boolean areSongsInDatabase) {
        this.areSongsInDatabase = areSongsInDatabase;
    }
}
