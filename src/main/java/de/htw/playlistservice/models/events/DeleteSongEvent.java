package de.htw.playlistservice.models.events;

public class DeleteSongEvent {

    private int songId;

    public DeleteSongEvent(int songId) {
        this.songId = songId;
    }

    public DeleteSongEvent() {
    }

    public int getSongId() {
        return songId;
    }

    public void setSongId(int songId) {
        this.songId = songId;
    }
}
