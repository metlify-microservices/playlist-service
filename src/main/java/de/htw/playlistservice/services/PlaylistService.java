package de.htw.playlistservice.services;



import de.htw.playlistservice.models.Playlist;
import de.htw.playlistservice.models.PlaylistDTO;
import de.htw.playlistservice.models.requests.*;

import java.util.List;
import java.util.Set;

public interface PlaylistService {
    boolean arePlaylistsInDatabase(Set<Integer> playListIds);
    Set<Playlist> getAllPlaylistsWithMultipleIds(Set<Integer> newLikedPlaylistIds);
    PlaylistDTO findPlaylistById(Integer id);
    List<PlaylistDTO> findPlaylistsByOwner(int user);
    List<PlaylistDTO> findAllPlaylists();
    PlaylistDTO createPlaylist(Playlist playlist);
    PlaylistDTO updateTitle(UpdatePlaylistTitleRequest request);
    PlaylistDTO updateGenre(UpdatePlaylistGenreRequest request);
    PlaylistDTO updateSongs(int id, Set<Integer> newSongs);
    PlaylistDTO updateIsPrivate(UpdatePlaylistIsPrivateRequest request);
    List<PlaylistDTO> getPlaylistsByParameter(String parameter, String value);
    void deletePlaylist(Integer id);

    void deletePlaylistsByUser(int userId);

    List<Playlist> findAllPlaylistsByOwner(int userId);

    void deletePlaylistSong(int songId);

}
