package de.htw.playlistservice.services;

import de.htw.playlistservice.models.events.*;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
public class QueueProducer implements MessagingService {


    private final RabbitTemplate template;
    private final DirectExchange directExchange;
    private static final String ROUTING_KEY_AUTH_HEADER = "userId.header.playlist";
    private static final String ROUTING_KEY_ARTIST_NAME = "userId.artistName";
    public static final String ROUTING_KEY_SONGS_IN_DB = "old.songs";
    public static final String ROUTING_KEY_RANDOM_SONGS = "random.songs";
    private static final String ROUTING_KEY_DELETE_PLAYLIST = "delete.playlist";


    public QueueProducer(RabbitTemplate template, @Qualifier("playlistsDirectExchange") DirectExchange directExchange) {
        this.template = template;
        this.directExchange = directExchange;
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public int getUserIdFromAuthHeader(String authHeader) {
        GetUserIdFromAuthHeaderEvent event = new GetUserIdFromAuthHeaderEvent(authHeader);
        System.out.println("sending userid from authheader request to user service");
        UserIdFromAuthHeader response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_AUTH_HEADER,
                event,
                new ParameterizedTypeReference<>() {
                });
        System.out.println("Got a response from userservice hopefully");
        if (response != null) {
            System.out.println("userid from user service" + response.getUserId());
            return response.getUserId();
        } else {
            return 0;
        }
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public int getUserIdByUsername(String owner) {
        GetUserIdFromArtistName event = new GetUserIdFromArtistName(owner);
        System.out.println("Sending userid from OwnernameReq to userservice");
        UserIdFromArtistName response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_ARTIST_NAME,
                event,
                new ParameterizedTypeReference<>() {
                });

        System.out.println("Got a response from userservice hopefully");

        if (response != null) {
            System.out.println("userid from user service ownername" + response.getUserId());

            return response.getUserId();
        } else {
            return 0;
        }
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public boolean areSongsInDatabase(Set<Integer> songIds) {
        AreSongsInDatabaseEvent event = new AreSongsInDatabaseEvent(songIds);

        SongsInDatabaseConfirmationEvent response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_SONGS_IN_DB,
                event,
                new ParameterizedTypeReference<>() {
                });

        if (response != null) {
            return response.isAreSongsInDatabase();
        } else {
            return false;
        }
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public List<Integer> getRandomSongsByGenre(String genre) {
        GetRandomSongsEvent event = new GetRandomSongsEvent(genre);

        RandomSongsEvent response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_RANDOM_SONGS,
                event,
                new ParameterizedTypeReference<>() {
                });

        if (response != null) {
            return response.getRandomSongIds();
        } else {
            return null;
        }
    }

    @Override
    public void deletePlaylistInAllServices(Integer id) {
        DeletePlaylistEvent event = new DeletePlaylistEvent(id);
        template.convertAndSend(directExchange.getName(), ROUTING_KEY_DELETE_PLAYLIST, event);
    }
}
