package de.htw.playlistservice.services;

import java.util.List;
import java.util.Set;

public interface MessagingService {
    int getUserIdFromAuthHeader(String authHeader);

    int getUserIdByUsername(String owner);

    boolean areSongsInDatabase(Set<Integer> songIds);

    List<Integer> getRandomSongsByGenre(String genre);

    void deletePlaylistInAllServices(Integer id);

//    Set<Integer> getAllSongsWithMultipleIds(Set<Integer> newSongs);
}
