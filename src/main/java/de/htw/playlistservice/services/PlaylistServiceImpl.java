package de.htw.playlistservice.services;


import de.htw.playlistservice.models.Playlist;
import de.htw.playlistservice.models.PlaylistDTO;
import de.htw.playlistservice.models.requests.*;
import de.htw.playlistservice.repositories.PlaylistRepository;
import de.htw.playlistservice.specifications.PlaylistSpecification;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    private final PlaylistRepository playlistRepository;
    private final boolean IS_NOT_PRIVATE = false;
    private final String DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";

    @Autowired
    public PlaylistServiceImpl(PlaylistRepository songRepository) {
        this.playlistRepository = songRepository;
    }


    @Override
    public List<PlaylistDTO> findAllPlaylists() {
        List<Playlist> playlists = playlistRepository.findAllByIsPrivate(IS_NOT_PRIVATE);
        List<PlaylistDTO> playlistDTOS = mapFromPlaylistsToPlaylistDTOs(playlists);
        return playlistDTOS;
    }

    private List<PlaylistDTO> mapFromPlaylistsToPlaylistDTOs(List<Playlist> playlists) {
        List<PlaylistDTO> playlistDTOS = new ArrayList<>();

        for (Playlist playlist: playlists) {
            PlaylistDTO playlistDTO = createPlaylistDTOFromPlaylist(playlist);

            playlistDTOS.add(playlistDTO);
        }
        return playlistDTOS;
    }


    @Override
    public List<PlaylistDTO> getPlaylistsByParameter(String parameter, String value) {
        PlaylistSpecification playlistSpecification = new PlaylistSpecification(parameter, value);
        List<Playlist> playlists = playlistRepository.findAll(playlistSpecification);
        List<Playlist> publicPlaylists = filterOutPrivatePlaylists(playlists);
        List<PlaylistDTO> playlistDTOS = mapFromPlaylistsToPlaylistDTOs(publicPlaylists);
        return playlistDTOS;
    }

    private List<Playlist> filterOutPrivatePlaylists(List<Playlist> playlists) {
        return playlists.stream().filter(song -> !song.isPrivate()).collect(Collectors.toList());
    }

    @Override
    public PlaylistDTO createPlaylist(Playlist playlist) {
        String updatedAt = getCurrentDateString();
        playlist.setLastUpdated(updatedAt);
        return createPlaylistDTOFromPlaylist(playlistRepository.save(playlist));
    }


    @Override
    public PlaylistDTO updateTitle(UpdatePlaylistTitleRequest request) {
        String newTitle = request.getNewTitle();
        Playlist playlistFromDB = playlistRepository.findById(request.getId());
        playlistFromDB.setTitle(newTitle);

        String updatedAt = getCurrentDateString();
        playlistFromDB.setLastUpdated(updatedAt);

        return createPlaylistDTOFromPlaylist(playlistRepository.save(playlistFromDB));
    }

    @Override
    public PlaylistDTO updateGenre(UpdatePlaylistGenreRequest request) {
        String newGenre = request.getNewGenre();
        Playlist playlistFromDB = playlistRepository.findById(request.getId());
        playlistFromDB.setGenre(newGenre);

        String updatedAt = getCurrentDateString();
        playlistFromDB.setLastUpdated(updatedAt);

        return createPlaylistDTOFromPlaylist(playlistRepository.save(playlistFromDB));
    }

    @Override
    public PlaylistDTO updateSongs(int playlistId, Set<Integer> newSongs) {
        playlistRepository.deletePlaylistSongsByPlaylistId(playlistId);
        for (Integer newSong : newSongs) {
            playlistRepository.addPlaylistSongs(playlistId, newSong);
        }
        return findPlaylistById(playlistId);
    }

    @Override
    public PlaylistDTO updateIsPrivate(UpdatePlaylistIsPrivateRequest request) {
        boolean isPrivate = request.isPrivate();
        Playlist playlistFromDB = playlistRepository.findById(request.getId());
        playlistFromDB.setPrivate(isPrivate);

        String updatedAt = getCurrentDateString();
        playlistFromDB.setLastUpdated(updatedAt);

        return createPlaylistDTOFromPlaylist(playlistRepository.save(playlistFromDB));
    }

    @Override
    public void deletePlaylist(Integer id) {
        playlistRepository.deleteById(id);
        playlistRepository.deletePlaylistSongsByPlaylistId(id);
    }

    @Override
    public void deletePlaylistsByUser(int userId) {
        playlistRepository.deleteByUserId(userId);
        List<Integer> playlistIds = getPlaylistIdsFromPlaylists();
        playlistRepository.deletePlaylistsSongs(playlistIds);
    }

    @Override
    public List<Playlist> findAllPlaylistsByOwner(int userId) {
        return playlistRepository.findByUserId(userId);
    }

    @Override
    public void deletePlaylistSong(int songId) {
        playlistRepository.deletePlaylistsSongsBySongId(songId);
    }

    private List<Integer> getPlaylistIdsFromPlaylists() {
        List<Playlist> playlists = playlistRepository.findAll();
        List<Integer> playlistIds = new ArrayList<>();
        for (Playlist playlist : playlists) {
            playlistIds.add(playlist.getId());
        }
        return playlistIds;
    }

    private String getCurrentDateString() {
        return new SimpleDateFormat(DATE_FORMAT).format(new Date());
    }

    @Override
    public PlaylistDTO findPlaylistById(Integer id) {
        Optional<Playlist> playlistOptional = playlistRepository.findById(id);
        if (playlistOptional.isEmpty()) {
            return null;
        } else {
            Playlist playlist = playlistOptional.get();
            PlaylistDTO playlistDTO = createPlaylistDTOFromPlaylist(playlist);
            return playlistDTO;
        }
    }

    private PlaylistDTO createPlaylistDTOFromPlaylist(Playlist playlist) {
        Integer playlistId = playlist.getId();

        Set<Integer> songs = playlistRepository.findSongsFromPlaylist(playlistId);
        ModelMapper modelMapper = new ModelMapper();
        PlaylistDTO playlistDTO = modelMapper.map(playlist, PlaylistDTO.class);
        playlistDTO.setSongs(songs);
        return playlistDTO;
    }

    @Override
    public List<PlaylistDTO> findPlaylistsByOwner(int user) {
        return mapFromPlaylistsToPlaylistDTOs(playlistRepository.findByUserIdAndIsPrivate(user, IS_NOT_PRIVATE));
    }

    @Override
    public boolean arePlaylistsInDatabase(Set<Integer> playListIds) {
        for (int id : playListIds) {
            if (!playlistRepository.existsById(id)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Set<Playlist> getAllPlaylistsWithMultipleIds(Set<Integer> playlistIds) {
        List<Playlist> playlistList = playlistRepository.findAllById(playlistIds);
        return new HashSet<>(playlistList);
    }
}
