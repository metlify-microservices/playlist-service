package de.htw.playlistservice.specifications;

import de.htw.playlistservice.models.Playlist;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class PlaylistSpecification implements Specification<Playlist> {

    private String variableColumnName;
    private Object valueToSearchFor;

    public PlaylistSpecification(String variableColumnName, Object valueToSearchFor) {
        this.variableColumnName = variableColumnName;
        this.valueToSearchFor = valueToSearchFor;
    }

    @Override
    public Predicate toPredicate(Root<Playlist> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(criteriaBuilder.equal(root.<String>get(this.variableColumnName), this.valueToSearchFor));
    }
}
