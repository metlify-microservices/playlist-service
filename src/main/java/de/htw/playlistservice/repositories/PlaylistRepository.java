package de.htw.playlistservice.repositories;

import de.htw.playlistservice.models.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface PlaylistRepository extends JpaRepository<Playlist, Integer>, JpaSpecificationExecutor<Playlist> {
    Playlist findById(int id);
    List<Playlist> findAllById(Iterable<Integer> ids);
    List<Playlist> findByTitleAndIsPrivate(String title, boolean isPrivate);
    List<Playlist> findByUserIdAndIsPrivate(int user, boolean isPrivate);
    List<Playlist> findByGenreAndIsPrivate(String genre, boolean isPrivate);
    List<Playlist> findAllByIsPrivate(boolean isPrivate);

    @Transactional
    void deleteByUserId(int userId);

    @Transactional
    @Modifying
    @Query(value = "DELETE from playlist_song where playlist_id NOT IN :#{#playlistIds}", nativeQuery = true)
    void deletePlaylistsSongs(@Param("playlistIds") List<Integer> playlistIds);

    List<Playlist> findByUserId(int userId);



    @Transactional
    @Modifying
    @Query(value="DELETE FROM playlist_song WHERE playlist_id = :#{#playlistId}", nativeQuery = true)
    void deletePlaylistSongsByPlaylistId(@Param("playlistId") int playlistId);
    @Transactional
    @Modifying
    @Query(value = "Insert into playlist_song values (:#{#playlistId},:#{#songId})", nativeQuery = true)
    void addPlaylistSongs(@Param("playlistId") int playlistId, @Param("songId") Integer songId);
    @Transactional
    @Modifying
    @Query(value="DELETE FROM playlist_song WHERE song_id = :#{#songId}", nativeQuery = true)
    void deletePlaylistsSongsBySongId(@Param("songId") int songId);
    @Query(value="SELECT song_id FROM playlist_song WHERE playlist_id = :#{#playlistId}", nativeQuery = true)
    Set<Integer> findSongsFromPlaylist(@Param("playlistId") Integer playlistId);
}
