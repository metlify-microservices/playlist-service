package de.htw.playlistservice.controllers;


import de.htw.playlistservice.models.Playlist;
import de.htw.playlistservice.models.PlaylistDTO;
import de.htw.playlistservice.models.requests.*;
import de.htw.playlistservice.services.MessagingService;
import de.htw.playlistservice.services.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/playlists")
public class PlaylistController {

    private final PlaylistService playlistService;
    private final MessagingService messagingService;


    @Autowired
    public PlaylistController(PlaylistService playlistService,
                              MessagingService messagingService) {
        this.playlistService = playlistService;
        this.messagingService = messagingService;
    }

    @GetMapping
    public ResponseEntity<List<PlaylistDTO>> getAllPlaylists() {
        List<PlaylistDTO> playlists = playlistService.findAllPlaylists();
        if (playlists.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(playlists);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<PlaylistDTO> getPlaylistById(@PathVariable("id") Integer id,
                                                       @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        PlaylistDTO playlist = playlistService.findPlaylistById(id);

        if (playlist == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else if (!playlist.isPrivate() || userIsAuthorized(playlist.getUserId(), authHeader)) {
            return ResponseEntity.status(HttpStatus.OK).body(playlist);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean userIsAuthorized(int id, String authHeader) {
        return id == messagingService.getUserIdFromAuthHeader(authHeader);
    }


    @GetMapping("/getBy")
    public ResponseEntity<List<PlaylistDTO>> getSongsByParameter(@RequestParam(name = "parameter") String parameter, @RequestParam(name = "value") String value) {
        try {
            List<PlaylistDTO> playlists = playlistService.getPlaylistsByParameter(parameter, value);

            if (playlists.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(playlists);
            }
        } catch (InvalidDataAccessApiUsageException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/owner")
    public ResponseEntity<List<PlaylistDTO>> getSongsByOwner(@RequestParam(name = "owner") String owner) {
        int user = messagingService.getUserIdByUsername(owner);
        System.out.println("user id drom userservice by owner " + user);

        List<PlaylistDTO> playlists = playlistService.findPlaylistsByOwner(user);

        if (playlists.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(playlists);
        }
    }

    @GetMapping("/generate")
    public ResponseEntity<PlaylistDTO> generatePlaylistByGenre(@RequestParam(name = "genre") String genre) {
        List<Integer> songs = messagingService.getRandomSongsByGenre(genre);
        System.out.println("random Songs from song service are empty?" + songs.isEmpty());
        System.out.println("random Songs from song service count?" + songs.size());
        if (!songs.isEmpty()) {
            PlaylistDTO playlist = new PlaylistDTO();
            playlist.setGenre(genre);
            playlist.setTitle(genre + " Playlist");
            playlist.setSongs(new HashSet<>(songs));

            return ResponseEntity.status(HttpStatus.OK).body(playlist);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    @PostMapping
    public ResponseEntity<PlaylistDTO> createPlaylist(@Valid @RequestBody Playlist playlist,
                                                      @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        int owner = messagingService.getUserIdFromAuthHeader(authHeader);
        playlist.setUserId(owner);
        PlaylistDTO addedPlaylist = playlistService.createPlaylist(playlist);

        return ResponseEntity.status(HttpStatus.OK).body(addedPlaylist);
    }

    @PutMapping("/title")
    public ResponseEntity<PlaylistDTO> updatePlaylistTitle(@Valid @RequestBody UpdatePlaylistTitleRequest request,
                                                           @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                PlaylistDTO updatedPlaylist = playlistService.updateTitle(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedPlaylist);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/genre")
    public ResponseEntity<PlaylistDTO> updatePlaylistGenre(@Valid @RequestBody UpdatePlaylistGenreRequest request,
                                                           @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                PlaylistDTO updatedPlaylist = playlistService.updateGenre(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedPlaylist);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    @PutMapping("/songs")
    public ResponseEntity<PlaylistDTO> updatePlaylistSongs(@Valid @RequestBody UpdatePlaylistSongsRequest request,
                                                           @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {

        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader) && songsAreInDatabase(request.getNewSongs())
                    && songsAreInDatabase(request.getNewSongs())) {
                PlaylistDTO updatedPlaylist = playlistService.updateSongs(request.getId(), request.getNewSongs());

                return ResponseEntity.status(HttpStatus.OK).body(updatedPlaylist);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/isPrivate")
    public ResponseEntity<PlaylistDTO> updatePlaylistIsPrivate(@Valid @RequestBody UpdatePlaylistIsPrivateRequest request,
                                                               @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(request.getId(), authHeader)) {
                PlaylistDTO updatedPlaylist = playlistService.updateIsPrivate(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedPlaylist);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Playlist> deletePlaylist(@PathVariable("id") Integer id,
                                                   @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorizedToUpdate(id, authHeader)) {
                playlistService.deletePlaylist(id);
                messagingService.deletePlaylistInAllServices(id);

                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (NullPointerException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    private boolean songsAreInDatabase(Set<Integer> songIds) {
        return messagingService.areSongsInDatabase(songIds);
    }


    private boolean userIsAuthorizedToUpdate(int id, String authHeader) {
        PlaylistDTO playlist = playlistService.findPlaylistById(id);
        int artistId = playlist.getUserId();

        return artistId == messagingService.getUserIdFromAuthHeader(authHeader);
    }

    // This is needed for elegant error message when bean validation fails on registration
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleError(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }
}
